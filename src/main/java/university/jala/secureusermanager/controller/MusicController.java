package university.jala.secureusermanager.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.secureusermanager.models.MusicEntity;
import university.jala.secureusermanager.models.dtos.MusicRequestDTO;
import university.jala.secureusermanager.repository.MusicRepository;
import university.jala.secureusermanager.services.MusicService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/music")
public class MusicController {

    @Autowired
    private MusicService musicService;

    @PostMapping
    public ResponseEntity<MusicEntity> createMusic(@RequestBody @Valid MusicRequestDTO music) {
        return musicService.saveMusic(music);
    }

    @GetMapping
    public ResponseEntity<List<MusicEntity>> getAllMusic() {
        return musicService.getAllMusic();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getMusicById(@PathVariable(value = "id") UUID id) {
        return musicService.getMusicById(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMusic(@PathVariable(value = "id") UUID id) {
        return musicService.deleteMusicById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateMusic(@PathVariable(value = "id") UUID id, @RequestBody @Valid MusicRequestDTO music) {
        return musicService.updateMusic(music, id);
    }
}