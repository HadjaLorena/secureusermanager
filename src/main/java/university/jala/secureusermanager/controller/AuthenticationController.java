package university.jala.secureusermanager.controller;

import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import university.jala.secureusermanager.models.UserEntity;
import university.jala.secureusermanager.models.dtos.AuthenticationDTO;
import university.jala.secureusermanager.models.dtos.RegisterDTO;
import university.jala.secureusermanager.services.TokenServices;
import university.jala.secureusermanager.services.UserService;

@RestController
@RequestMapping("/user")
public class AuthenticationController {
    @Autowired
    TokenServices tokenServices;

    private AuthenticationManager authenticationManager;
    private UserService userService;

    public AuthenticationController(AuthenticationManager authenticationManager, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody @Valid AuthenticationDTO data){
        var usernamePassword = new UsernamePasswordAuthenticationToken(data.login(),data.password());
        var auth = this.authenticationManager.authenticate(usernamePassword);

        var token = tokenServices.getToken((UserEntity) auth.getPrincipal());
        return ResponseEntity.status(HttpStatus.OK).body(token);
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody @Valid RegisterDTO registerDTO){
        if (userService.existsByUsername(registerDTO.login())) return  ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        String encryptedPassword = new BCryptPasswordEncoder().encode(registerDTO.password());
        UserEntity newUser = new UserEntity();
        BeanUtils.copyProperties(registerDTO, newUser);
        newUser.setPassword(encryptedPassword);
        userService.save(newUser);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}