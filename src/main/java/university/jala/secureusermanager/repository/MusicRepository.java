package university.jala.secureusermanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import university.jala.secureusermanager.models.MusicEntity;

import java.util.UUID;

@Repository
public interface MusicRepository extends JpaRepository<MusicEntity, UUID> {
}