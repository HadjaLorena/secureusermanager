package university.jala.secureusermanager.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import university.jala.secureusermanager.models.UserEntity;
import university.jala.secureusermanager.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByLogin(username);
    }

    public boolean existsByUsername(String username) {
        return userRepository.existsByLogin(username);
    }

    public void save(UserEntity user) {
        userRepository.save(user);
    }
}