package university.jala.secureusermanager.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import university.jala.secureusermanager.models.UserEntity;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class TokenServices {
    @Value("${api.security.token.secret}")
    private String secret;

    public String getToken(UserEntity userEntity) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            String token = JWT.create().withIssuer("SecureUserManager").withSubject(userEntity.getLogin()).withExpiresAt(generationExpirationData()).sign(algorithm);
            return token;

        }catch (JWTCreationException e){
            throw new RuntimeException("Error while creating token", e);
        }
    }

    private Instant generationExpirationData(){
        return LocalDateTime.now().plusHours(2).toInstant(ZoneOffset.of("-03:00"));
    }

    public String validationToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            return JWT.require(algorithm).withIssuer("SecureUserManager").build().verify(token).getSubject();

        }
        catch (JWTCreationException e){
            return "Invalid token";
        }
    }
}