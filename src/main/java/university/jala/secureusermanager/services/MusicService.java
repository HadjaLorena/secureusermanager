package university.jala.secureusermanager.services;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import university.jala.secureusermanager.models.MusicEntity;
import university.jala.secureusermanager.models.dtos.MusicRequestDTO;
import university.jala.secureusermanager.repository.MusicRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MusicService {
    @Autowired
    private MusicRepository musicRepository;

    public ResponseEntity<MusicEntity> saveMusic(MusicRequestDTO music) {
        MusicEntity musicEntity = new MusicEntity();
        BeanUtils.copyProperties(music, musicEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(musicRepository.save(musicEntity));
    }

    public ResponseEntity<List<MusicEntity>> getAllMusic() {
        return ResponseEntity.status(HttpStatus.OK).body(musicRepository.findAll());
    }

    public ResponseEntity<Object> getMusicById(UUID id) {
        Optional<MusicEntity> musicEntity = musicRepository.findById(id);

        if(musicEntity.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Music not found");
        }
        return ResponseEntity.status(HttpStatus.OK).body(musicEntity.get());
    }

    public ResponseEntity<String> deleteMusicById(UUID id) {
        Optional<MusicEntity> musicEntity = musicRepository.findById(id);

        if(musicEntity.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Music not found");
        }
        musicRepository.delete(musicEntity.get());
        return ResponseEntity.status(HttpStatus.OK).body("Music deleted successfully");
    }

    public ResponseEntity<Object> updateMusic(MusicRequestDTO music, UUID id) {
        Optional<MusicEntity> musicEntity = musicRepository.findById(id);

        if(musicEntity.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Music not found");
        }
        MusicEntity newMusicEntity = musicEntity.get();
        BeanUtils.copyProperties(music, newMusicEntity);
        newMusicEntity.setUpdatedAt(new Date());
        return ResponseEntity.status(HttpStatus.OK).body(musicRepository.save(newMusicEntity));
    }
}