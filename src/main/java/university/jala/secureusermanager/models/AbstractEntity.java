package university.jala.secureusermanager.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    AbstractEntity(){
        Date now = new Date();
        CreatedAt = now;
        UpdatedAt = now;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "ID")
    private UUID Id;
    @Column(name = "DT_CREATED_AT")
    private final Date CreatedAt;
    @Column(name = "DT_UPDATED_AT")
    private Date UpdatedAt;
}
