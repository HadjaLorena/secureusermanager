package university.jala.secureusermanager.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import jakarta.persistence.Column;

@Entity
@Table(name = "MusicTable")
@Getter
@Setter
public class MusicEntity extends AbstractEntity {
    @Column(name ="music_title")
    private String title;

    @Column(name="music_artist")
    private String artist;

    @Column(name="music_genre")
    private String gender;

    @Column(name="music_duration")
    private Double duration;
}