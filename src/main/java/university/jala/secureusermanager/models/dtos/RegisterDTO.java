package university.jala.secureusermanager.models.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import university.jala.secureusermanager.models.UserRole;

public record RegisterDTO(
        @NotBlank @NotNull String login,
        @NotBlank @NotNull String password,
        @NotNull UserRole role
) { }