package university.jala.secureusermanager.models.dtos;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record MusicRequestDTO(
        @NotBlank @NotNull String title,
        @NotBlank @NotNull String artist,
        @NotBlank @NotNull String gender,
        @NotNull Double duration
        ) {
}